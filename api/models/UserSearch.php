<?php

namespace api\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

class UserSearch extends User
{
    public function rules()
    {
        return [
            [['username', 'email', 'first_name', 'last_name', 'birthday', 'device_id', 'user_type'], 'required'],
            ['email', 'email'],
            [['description', 'phone', 'latitude', 'longitude', 'phone', 'facebook_profile_id', 'avatar'], 'string'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'description', $this->description]);
        $query->andFilterWhere(['like', 'phone', $this->phone]);
        $query->andFilterWhere(['like', 'latitude', $this->latitude]);
        $query->andFilterWhere(['like', 'longitude', $this->longitude]);
        $query->andFilterWhere(['like', 'phone', $this->phone]);
        $query->andFilterWhere(['like', 'facebook_profile_id', $this->facebook_profile_id]);
        $query->andFilterWhere(['like', 'avatar', $this->avatar]);

        return $dataProvider;
    }

    public function formName()
    {
        return 's';
    }
}
