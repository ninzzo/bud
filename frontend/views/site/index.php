<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<section class="intro-sec">
      <div class="container">
          <div class="row">
              <div class="col-12 col-sm-6 ">
                  <div class="intro-content">
                      <h1>Download the app</h1>
                      <span>Bud Meow!</span>
                  </div>
                  <div class="apps-icons">
                      <ul class="app-list">
                        <li><a href="#"><img  class="img-fluid" src="http://workassign.org/webfront_land_retailer/images/app-logo.png"></a></li>
                        <li><a href="#"><img  class="img-fluid" src="http://workassign.org/webfront_land_retailer/images/google-app-icon.png"></a></li>
                      </ul>
                  </div>
              </div>
              <div class="col-12 col-sm-6 mobile-holder">
                <div class="mobile-view">
                  <img class="img-fluid" src="http://workassign.org/webfront_land_retailer/images/site-mobile-view.png">
                </div>
              </div>
          </div>
      </div>
  </section>

  <section class="driver-sec text-center">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="title-head"><span>DRIVERS</span></h2>

          <div class="invite-intro">
            <h2>READY TO DRIVE MEOW ?</h2>
            <span>Download the fleet app below!</span>
            <div class="apps-icons">
                      <ul class="app-list">
                        <li><a href="#"><img  class="img-fluid" src="http://workassign.org/webfront_land_retailer/images/app-logo.png"></a></li>
                        <li><a href="#"><img  class="img-fluid" src="http://workassign.org/webfront_land_retailer/images/google-app-icon.png"></a></li>
                      </ul>
                  </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  
 
  