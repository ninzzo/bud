<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

//AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="daZ55N4F8SaBqyMctHNPdwaaxj21a9EiRcymTvbc">
    <link rel="stylesheet" href="http://workassign.org/webfront_land_retailer/pre-css/bootstrap.min.css">
    <link rel="stylesheet" href="http://workassign.org/webfront_land_retailer/pre-css/main.css">
    <link rel="stylesheet" href="http://workassign.org/webfront_land_retailer/pre-css/bootstrap-select.min.css" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
</head>
<body>
<?php $this->beginBody() ?>

 <header id="header" class="main-header">
      <div class="container">       
        <nav class="navbar navbar-expand-lg navbar-light">
          <a class="navbar-brand logo" href="#"><img src="http://workassign.org/webfront_land_retailer/images/site-logo.png"></a>
          <button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarsExample09">
            <ul class="navbar-nav ml-auto">
               <li class="nav-item active">
                <a class="nav-link" href="/frontend/web/home">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">What is budmeow</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Contact</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Faqs</a>
              </li>
            </ul>            
          </div>       
        
        </nav> 
    </div>
  </header>

    
<?= $content ?>
    

 
    <footer>
    <div class="foot-top">
        <div class="container">
          <div class="row">
                <div class="col-md-12 text-center">
                    <a href="/frontend/web/dashboard" class="btn btn-default Vendor-btn">Vendors</a>
                    <ul class="foot-nav">
                      <li><a class="sign-up" href="/frontend/web/register">Sign Up</a></li>
                      <li><a href="/frontend/web/login">Login</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
  </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="http://workassign.org/webfront_land_retailer/js/popper.min.js" ></script>
    <script src="http://workassign.org/webfront_land_retailer/js/bootstrap.min.js"></script>
    <script src="http://workassign.org/webfront_land_retailer/js/bootstrap-select.min.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
