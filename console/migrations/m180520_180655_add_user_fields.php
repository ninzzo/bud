<?php

use yii\db\Migration;

class m180520_180655_add_user_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'first_name', $this->string(256)->notNull());
        $this->addColumn('{{%user}}', 'last_name', $this->string(256)->notNull());
        $this->addColumn('{{%user}}', 'birthday', $this->integer(11)->notNull());
        $this->addColumn('{{%user}}', 'phone', $this->string(128));
        $this->addColumn('{{%user}}', 'latitude', $this->string(128));
        $this->addColumn('{{%user}}', 'longitude', $this->string(128));
        $this->addColumn('{{%user}}', 'device_id', $this->string(128)->notNull());
        $this->addColumn('{{%user}}', 'user_type', $this->integer(5)->notNull());
        $this->addColumn('{{%user}}', 'facebook_profile_id', $this->string(256));
        $this->addColumn('{{%user}}', 'avatar', $this->string(256));        
    }

    public function down()
    {
        
    }
}
