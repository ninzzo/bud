<?php

use yii\db\Migration;

/**
 * Handles the creation of table `retailer`.
 */
class m180520_204913_create_retailer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%retailer}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'address' => $this->string(),
            'site' => $this->string(),
            'work_time' => $this->string(),
            'logo' => $this->string(),
        ], $tableOptions);

        $this->createIndex('idx-retailer-user_id', '{{%retailer}}', 'user_id');

        $this->addForeignKey('fk-retailer-user_id', '{{%retailer}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('retailer');
    }

   

    
}
